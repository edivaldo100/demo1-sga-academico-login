package br.com.sga.dao;

import br.com.sga.model.User;

public interface UserDao {
 
    void save(User user);
     
    User findById(int id);
     
    User findBySSO(String sso);
     
}
