package br.com.sga.dao;


import java.util.List;

import br.com.sga.model.UserProfile;
  
public interface UserProfileDao {
 
    List<UserProfile> findAll();
     
    UserProfile findByType(String type);
     
    UserProfile findById(int id);
}
