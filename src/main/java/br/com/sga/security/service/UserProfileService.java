package br.com.sga.security.service;


import java.util.List;
 
import br.com.sga.model.UserProfile;
 
public interface UserProfileService {
 
    List<UserProfile> findAll();
     
    UserProfile findByType(String type);
     
    UserProfile findById(int id);
}
