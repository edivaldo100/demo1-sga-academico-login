package br.com.sga.security.service;

import br.com.sga.model.User;

public interface UserService {
 
    void save(User user);
     
    User findById(int id);
     
    User findBySso(String sso);
     
}
